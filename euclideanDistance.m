function [dst] = euclideanDistance(point1, point2)
    len = size(point1, 1);
    dst = 0;
    
    for i = 1 : len
        diff = point1(i) - point2(i);
        dst = dst + diff * diff;
    end
    
    dst = sqrt(dst);
end