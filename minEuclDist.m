function [predLabels] = minEuclDist(trainSet, labels, testSet)
    trainLen = size(trainSet, 2);
    testLen = size(testSet, 2);
    dim = size(trainSet, 1);
    means = zeros(dim, 5); predLabels = [];
    
    % calculate parameters for every class and every feature
    for i = 1 : trainLen
        for j = 1 : dim
            means(j, labels(i)) = means(j, labels(i)) + ...
                                  trainSet(j, i) / sum(labels == labels(i));
        end
    end
    
    % predict the classes for the testSet
    for i = 1 : testLen
        minim = -1; lab = 0;
        
        for j = 1 : 5
            dist = euclideanDistance(testSet(:, i), means(:, j));

            if minim == -1 || minim > dist
                minim = dist;
                lab = j;
            end
        end
        
        predLabels = [predLabels lab];
    end
end