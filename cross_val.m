function [bestAccur, bestKnnK] = cross_val(trainSet, labels)
    bestKnnK = 0; bestAccur = 0;
    
    for knnK = 1 : 17
        if mod(knnK, 2) == 0 % skip even values
            continue
        end
        
        K = 5; accur = 0;
        indices = cvpartition(labels, 'KFold', K); % stratified KFold
        
        % Apply 5-fold for a specific knnK
        for i = 1 : K
            trainInd = indices.training(i);
            testInd = indices.test(i);

            kfTrainSet = trainSet(:, trainInd == 1);
            kfTrainLabels = labels(trainInd == 1);

            kfTestSet = trainSet(:, testInd == 1);
            kfTestLabels = labels(testInd == 1);

            predLabels = knn(kfTrainSet, kfTrainLabels, kfTestSet, knnK);
            accur = accur + sum(predLabels == kfTestLabels) / indices.TestSize(i);
        end
        
        accur = accur / K;
        
        % Pick the best odd knnK from the range [1, 17]
        if bestAccur < accur
            bestAccur = accur;
            bestKnnK = knnK;
        end
    end
end