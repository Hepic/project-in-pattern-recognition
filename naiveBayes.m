function [predLabels] = naiveBayes(trainSet, labels, testSet)
    trainLen = size(trainSet, 2);
    testLen = size(testSet, 2);
    dim = size(trainSet, 1);
    priors = zeros(1, 5);
    means = zeros(dim, 5); variance = zeros(dim, 5);
    predLabels = [];
    
    % calculate parameters for every class and every feature
    for i = 1 : trainLen
        priors(labels(i)) = priors(labels(i)) + 1 / trainLen;

        for j = 1 : dim
            means(j, labels(i)) = means(j, labels(i)) + ...
                                  trainSet(j, i) / sum(labels == labels(i));
        end
    end
    
    for i = 1 : trainLen
        for j = 1 : dim
            diff = trainSet(j, i) - means(j, labels(i));
            variance(j, labels(i)) = variance(j, labels(i)) + ...
                                     (diff * diff) / sum(labels == labels(i));
        end
    end
    
    % predict the classes for the testSet
    for i = 1 : testLen
        maxim = 0; lab = 0;
        
        % use log to avoid underflow
        for k = 1 : 5
            curr = log(priors(k));
            
            for j = 1 : dim
                val = log(1 / sqrt(2 * pi * variance(j, k))) + ...
                      (-(testSet(j, i) - means(j, k))^2 / (2 * variance(j, k)));
                
                curr = curr + val;
            end
          
            if lab == 0 || maxim < curr
                maxim = curr;
                lab = k;
            end
        end
        
        predLabels = [predLabels lab];
    end
end