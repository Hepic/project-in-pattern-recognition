% This is a supporting MATLAB file for the project

clear
format compact
close all

load Salinas_hyperspectral %Load the Salinas hypercube called "Salinas_Image"
[p,n,l]=size(Salinas_Image); % p,n define the spatial resolution of the image, while l is the number of bands (number of features for each pixel)

load classification_labels 
% This file contains three arrays of dimension 22500x1 each, called
% "Training_Set", "Test_Set" and "Operational_Set". In order to bring them
% in an 150x150 image format we use the command "reshape" as follows:
Training_Set_Image=reshape(Training_Set, p,n); % In our case p=n=150 (spatial dimensions of the Salinas image).
Test_Set_Image=reshape(Test_Set, p,n);
Operational_Set_Image=reshape(Operational_Set, p,n);

%Depicting the various bands of the Salinas image
%for i=1:l
   % figure(1), imagesc(Salinas_Image(:,:,i))
   % pause(0.05) % This command freezes figure(1) for 0.05sec. 
%end

% Depicting the training, test and operational sets of pixels (for the
% pixels depicted with a dark blue color, the class label is not known.
% Each one of the other colors in the following figures indicate a class).
%figure(2), imagesc(Training_Set_Image)
%figure(3), imagesc(Test_Set_Image)
%figure(4), imagesc(Operational_Set_Image)

% Constructing the 204xN array whose columns are the vectors corresponding to the
% N vectors (pixels) of the training set (similar codes cane be used for
% the test and the operational sets).
Train=zeros(p,n,l); % This is a 3-dim array, which will contain nonzero values only for the training pixels
for i=1:l
     %Multiply elementwise each band of the Salinas_Image with the mask 
     % "Training_Set_Image>0", which identifies only the training vectors.
    Train(:,:,i)=Salinas_Image(:,:,i).*(Training_Set_Image>0);
    %figure(5), imagesc(Train(:,:,i)) % Depict the training set per band
    %pause(0.05)
end

Test=zeros(p,n,l); % This is a 3-dim array, which will contain nonzero values only for the test pixels
for i=1:l
     %Multiply elementwise each band of the Salinas_Image with the mask 
     % "Test_Set_Image>0", which identifies only the test vectors.
    Test(:,:,i)=Salinas_Image(:,:,i).*(Test_Set_Image>0);
 %   figure(6), imagesc(Test(:,:,i)) % Depict the test set per band
    %pause(0.05)
end

Operational=zeros(p,n,l); % This is a 3-dim array, which will contain nonzero values only for the operational pixels
for i=1:l
     %Multiply elementwise each band of the Salinas_Image with the mask 
     % "Operational_Set_Image>0", which identifies only the operational vectors.
    Operational(:,:,i)=Salinas_Image(:,:,i).*(Operational_Set_Image>0);
 %   figure(7), imagesc(Operational(:,:,i)) % Depict the operational set per band
    %pause(0.05)
end

Train_array=[]; %This is the wanted 204xN array
Train_array_response=[]; % This vector keeps the label of each of the training pixels
Train_array_pos=[]; % This array keeps (in its rows) the position of the training pixels in the image.
for i=1:p
    for j=1:n
        if(Training_Set_Image(i,j)>0) %Check if the (i,j) pixel is a training pixel
            Train_array=[Train_array squeeze(Train(i,j,:))];
            Train_array_response=[Train_array_response Training_Set_Image(i,j)];
            Train_array_pos=[Train_array_pos; i j];
        end
    end
end

Test_array=[]; %This is the wanted 204xN array
Test_array_response=[]; % This vector keeps the label of each of the test pixels
Test_array_pos=[]; % This array keeps (in its rows) the position of the test pixels in the image.
for i=1:p
    for j=1:n
        if(Test_Set_Image(i,j)>0) %Check if the (i,j) pixel is a test pixel
            Test_array=[Test_array squeeze(Test(i,j,:))];
            Test_array_response=[Test_array_response Test_Set_Image(i,j)];
            Test_array_pos=[Test_array_pos; i j];
        end
    end
end

Operational_array=[]; %This is the wanted 204xN array
Operational_array_response=[]; % This vector keeps the label of each of the operational pixels
Operational_array_pos=[]; % This array keeps (in its rows) the position of the operational pixels in the image.
for i=1:p
    for j=1:n
        if(Operational_Set_Image(i,j)>0) %Check if the (i,j) pixel is a operational pixel
            Operational_array=[Operational_array squeeze(Operational(i,j,:))];
            Operational_array_response=[Operational_array_response Operational_Set_Image(i,j)];
            Operational_array_pos=[Operational_array_pos; i j];
        end
    end
end

colors = ['bo'; 'yo'; 'ro'; 'go'; 'mo'];

figure(8), 
    hold on;
    for i = 1 : 5
        plot(-Train_array_pos(Train_array_response == i, 1),  ...
             Train_array_pos(Train_array_response == i, 2), ...
             colors(i, :));
    end
    
    for i = 1 : 5
        plot(-Test_array_pos(Test_array_response == i, 1),  ...
             Test_array_pos(Test_array_response == i, 2), ...
             colors(i, :));
    end
    
    for i = 1 : 5
        plot(-Operational_array_pos(Operational_array_response == i, 1),  ...
             Operational_array_pos(Operational_array_response == i, 2), ...
             colors(i, :));
    end
    hold off;
figure(8), axis equal

% KNN classifier
[crossAccur, knnK] = cross_val(Train_array, Train_array_response)
predLabels = knn(Train_array, Train_array_response, Test_array, knnK);
testAccur = 0;
confMatr = zeros(5, 5);

% compute the confusion matrix
for i = 1 : size(Test_array, 2)
    p1 = Test_array_response(i); p2 = predLabels(i);
    confMatr(p2, p1) = confMatr(p2, p1) + 1;
end

% compute the accuracy for the testset
for i = 1 : 5
    testAccur = testAccur + confMatr(i, i);
end

testAccur = testAccur / size(Test_array, 2)

figure(9),
    hold on;
    for i = 1 : 5
        plot(-Test_array_pos(predLabels == i, 1),  ...
             Test_array_pos(predLabels == i, 2), ...
             colors(i, :));
    end
    hold off;
figure(9), axis equal

% Naive Bayes classifier
predLabels = naiveBayes(Train_array, Train_array_response, Test_array);
testAccur = 0;
confMatr = zeros(5, 5);

% compute the confusion matrix
for i = 1 : size(Test_array, 2)
    p1 = Test_array_response(i); p2 = predLabels(i);
    confMatr(p2, p1) = confMatr(p2, p1) + 1;
end

% compute the accuracy for the testset
for i = 1 : 5
    testAccur = testAccur + confMatr(i, i);
end

testAccur = testAccur / size(Test_array, 2)

figure(10),
    hold on;
    for i = 1 : 5
        plot(-Test_array_pos(predLabels == i, 1),  ...
             Test_array_pos(predLabels == i, 2), ...
             colors(i, :));
    end
    hold off;
figure(10), axis equal

% Minimum Euclidean Distance classifier
predLabels = minEuclDist(Train_array, Train_array_response, Test_array);
testAccur = 0;
confMatr = zeros(5, 5);

% compute the confusion matrix
for i = 1 : size(Test_array, 2)
    p1 = Test_array_response(i); p2 = predLabels(i);
    confMatr(p2, p1) = confMatr(p2, p1) + 1;
end

% compute the accuracy for the testset
for i = 1 : 5
    testAccur = testAccur + confMatr(i, i);
end

testAccur = testAccur / size(Test_array, 2)

figure(11),
    hold on;
    for i = 1 : 5
        plot(-Test_array_pos(predLabels == i, 1),  ...
             Test_array_pos(predLabels == i, 2), ...
             colors(i, :));
    end
    hold off;
figure(11), axis equal