function [predLabels] = knn(trainSet, labels, testSet, K)
    trainLen = size(trainSet, 2);
    testLen = size(testSet, 2);
    predLabels = [];
    
    for i = 1 : testLen
        allDists = [];
        
        % find all distances
        for j = 1 : trainLen
            dist = euclideanDistance(testSet(:, i), trainSet(:, j));
            allDists = [allDists; dist j];
        end
        
        allDists = sortrows(allDists);
        kLabels = [];
        
        % find K nearest labels
        for j = 1 : K
            pos = allDists(j, 2);
            kLabels = [kLabels labels(pos)];
        end
        
        kLabels = sort(kLabels);
        cnt = 0; maxim = 0; lab = 0;
        
        % find the most common label
        for j = 1 : K
            if j == 1 || kLabels(j) ~= kLabels(j - 1)
                cnt = 1;
            else
                cnt = cnt + 1;
            end
            
            if maxim < cnt
                maxim = cnt;
                lab = kLabels(j);
            end
        end
        
        predLabels = [predLabels lab];
    end
end